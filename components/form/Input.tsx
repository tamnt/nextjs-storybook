import React from 'react';
import './style.scss';

interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {

}
export default (props: InputProps) => (
  <div className="form-group">
    <input className="form-input" type={props.type} onChange={props.onChange} />
  </div>
)