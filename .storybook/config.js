import { configure, addParameters, addDecorator } from '@storybook/react';
import { setConsoleOptions } from '@storybook/addon-console';
import { withKnobs } from '@storybook/addon-knobs';
import { withOptions } from '@storybook/addon-options';

configure(require.context('../stories', true, /\.stories\.tsx$/), module);

addParameters({
  viewport: {
    defaultViewport: 'iphonex'
  }
})

addDecorator(withKnobs)

addDecorator(withOptions({
  name: 'Story Components',
  addonPanelInRight: true
}));

setConsoleOptions({
  panelExclude: [],
});