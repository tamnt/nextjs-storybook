import React from 'react';
import Input from '../components/form/Input';
import { storiesOf } from '@storybook/react';
import { text, boolean, number } from '@storybook/addon-knobs';

export default {
  title: 'Form'
}

storiesOf('Form', module)
  // .addDecorator(withInfo)
  .add(
    'Input',
    () => <Input type={text("type","text")} onChange={({target}:any) => console.log(target.value)} />
  )