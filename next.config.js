const withTypescript = require('@zeit/next-typescript')
const withCSS = require("@zeit/next-css");
const withSass = require('@zeit/next-sass')

module.exports = withTypescript({
  webpack(config, options) {
    return config
  }
})

module.exports = withCSS({});

module.exports = withSass({
  sassLoaderOptions: {
    includePaths: ["components/**/*"]
  }
})