import React from 'react'
import Head from 'next/head'
import Input from '../components/form/Input'

const Home = () => (
  <div>
    <Head>
      <title>Home</title>
    </Head>

    <Input type="text" onChange={(e:any) => console.log(e.target.value)} />
  </div>
)

export default Home
